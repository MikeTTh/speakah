package main

import (
	"fmt"
	"os/exec"
	"time"
)

func say(s, lang string) {
	cmd := exec.Command("speak", "-s", "120", "-v", lang, s)
	running := false
	go func() {
		time.Sleep(20 * time.Second)
		if running {
			_ = cmd.Process.Kill()
		}
	}()
	running = true
	_, e := cmd.CombinedOutput()
	if e != nil {
		fmt.Println(e)
		return
	}
	running = false
}

func byLetter(s string) string {
	ret := ""
	for _, l := range []rune(s) {
		ret += string(l) + " "
	}
	return ret
}
