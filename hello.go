package main

import (
	"fmt"
	"os"
	"time"
)

func hello() {
	say("Köszönöm, hogy bekapcsoltál", "hu")
	time.Sleep(500 * time.Millisecond)
	say("i love", "en")
	say("káeszká", "hu")
	time.Sleep(time.Second)
	name := "computer"
	n, e := os.Hostname()
	if e == nil {
		name = n
	}
	say(fmt.Sprintf("%s goes burrrrrrrrrrrrrrrrrr", name), "en")
}
