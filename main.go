package main

import "sync"

func main() {
	hello()
	docker()
	interfaces()

	bye()

	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()
}
