package main

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"time"
)

var runningContainers = make(map[string]string)

func docker (){
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		say("docker failed to start", "en")
		return
	}

	containers, e := cli.ContainerList(ctx, types.ContainerListOptions{})
	if e != nil {
		say("can't list containers", "en")
		return
	}

	if len(containers) == 0 {
		say("No running containers. Docker is sad and lonely.", "en")
	} else {
		say("Running containers: ", "en")
	}
	for _, container := range containers {
		if container.State == "running" {
			say(container.Names[0][1:], "en")
			runningContainers[container.ID] = container.Names[0][1:]
		}
	}

	go func() {
		for {
			containers, e := cli.ContainerList(ctx, types.ContainerListOptions{})
			if e != nil {
				say("can't list containers", "en")
			}

			run := make(map[string]byte)

			for _, container := range containers {
				_, ok := runningContainers[container.ID]
				if !ok && container.State == "running" {
					say(fmt.Sprintf("Container %s started", container.Names[0][1:]), "en")
					runningContainers[container.ID] = container.Names[0][1:]
					run[container.ID] = 1
					continue
				} else if container.State != "running" {
					say(fmt.Sprintf("Container %s stopped", container.Names[0][1:]), "en")
					delete(runningContainers, container.ID)
					continue
				} else {
					run[container.ID] = 1
				}
			}

			toDelete := make([]string, 0)

			for k, v := range runningContainers {
				if _, ok := run[k]; !ok {
					say(fmt.Sprintf("Container %s stopped", v), "en")
					toDelete = append(toDelete, k)
				}
			}

			for _, v := range toDelete {
				delete(runningContainers, v)
			}
			time.Sleep(time.Second)
		}
	}()
}
