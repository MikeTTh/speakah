package main

import (
	"fmt"
	"net"
	"strings"
	"time"
)

type white []string

var whitelist white = []string{
	"eno",
	"enp",
	"wlp",
}

func (w *white) Matches(s string) bool {
	for _, wh := range *w {
		if strings.Contains(s, wh) {
			return true
		}
	}
	return false
}

var ifs = make(map[string]bool)

func getInterfaces() ([]net.Interface, error) {
	ifs, e := net.Interfaces()
	if e != nil {
		return ifs, e
	}

	validIfs := make([]net.Interface, 0, len(ifs))

	for _, i := range ifs {
		if whitelist.Matches(i.Name) {
			validIfs = append(validIfs, i)
		}
	}

	return validIfs, nil
}

func interfaces() {
	inter, e := getInterfaces()
	if e != nil {
		say("Could not list interfaces", "en")
		return
	}

	for _, i := range inter {
		ifs[i.Name] = false
	}

	go func() {
		for {
			inter, e := getInterfaces()
			if e != nil {
				say("Could not list interfaces", "en")
				return
			}

			for _, i := range inter {
				up := i.Flags&net.FlagUp != 0
				if up {
					addr, e := i.Addrs()
					if e != nil {
						up = false
					} else {
						if len(addr) == 0 {
							up = false
						}
					}
				}
				if up {
					if !ifs[i.Name] {
						say(fmt.Sprintf("Interface %s up", byLetter(i.Name)), "en")
					}
					ifs[i.Name] = true
				} else {
					if ifs[i.Name] {
						say(fmt.Sprintf("Interface %s down", byLetter(i.Name)), "en")
					}
					ifs[i.Name] = false
				}
			}
			time.Sleep(time.Second)
		}
	}()
}
