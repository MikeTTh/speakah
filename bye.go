package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func bye() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGINT, syscall.SIGHUP)

	go func() {
		select {
		case sig := <-c:
			_ = sig
			name := "computer"
			n, e := os.Hostname()
			if e == nil {
				name = n
			}
			say(fmt.Sprintf("%s is shutting down. Bye-bye.", name), "en")
			os.Exit(0)
		}
	}()
}
